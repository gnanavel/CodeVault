
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Valeriu Codreanu <valeriu.codreanu@surfsara.nl>
#
# ==================================================================================================
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../../../../cmake/Modules")

set(CMAKE_VERBOSE_MAKEFILE ON)


## OGUZ: Edit accordingly
include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 2_sparse)
endif()

## OGUZ: Add executables. There must be more convenient way :-)
set(NAME ${DWARF_PREFIX}_krylov_simple)
set(NAMEB ${DWARF_PREFIX}_krylov_twosys)
set(NAMEC ${DWARF_PREFIX}_krylov_multirhs)

# ==================================================================================================
# C++ compiler settings

find_package(Common)
find_package(PETSc)
find_package(MPI)

select_compiler_flags(cxx_flags
  GNU "-march=native"   # I suggest remove "-O3" as this is controlled by the CMAKE_BUILD_TYPE
  CLANG "-march=native" # same here
  Intel "-axavx2,avx")
set(CXX_FLAGS ${cxx_flags})
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set(CXX_FLAGS "${CXX_FLAGS} -Wall -Wno-comment")
  if(APPLE)
    set(CXX_FLAGS "${CXX_FLAGS} -Wa,-q")
  endif()
endif()
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS}")

# ==================================================================================================

# LUD with the MKL library
if (PETSc_FOUND AND MPI_FOUND)
	message("Petsc dir: ${PETSC_DIR}")
	message("Petsc arch: ${PETSC_ARCH}")
	message("Petsc includes: ${PETSC_INCLUDES}")
	message("Petsc libs: ${PETSC_LIBRARIES}")
	include_directories(${PETSC_INCLUDES})
	add_definitions (${PETSC_DEFINITIONS})
	link_directories(${petsc_lib_dir})
	
	add_executable(${NAME} ksp_solver_simple.c)
	target_link_libraries(${NAME} ${PETSC_LIBRARIES})
	install(TARGETS ${NAME} DESTINATION bin)
	
	add_executable(${NAMEB} ksp_solver_two_sys.c)
	target_link_libraries(${NAMEB} ${PETSC_LIBRARIES} mpi)
	install(TARGETS ${NAMEB} DESTINATION bin)
	
	add_executable(${NAMEC} ksp_solver_multi_rhs.c)
	target_link_libraries(${NAMEC} ${PETSC_LIBRARIES} mpi)
	install(TARGETS ${NAMEC} DESTINATION bin)
else ()
	message("## Skipping '${NAME}': no PETSC support found")
	install(CODE "MESSAGE(\"${NAME} can only be built with PETSC.\")")
endif()

unset(NAME)

# ==================================================================================================
