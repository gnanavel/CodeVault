#include "petsc.h"
#include "petscfix.h"
/* snesmfjdef.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscsnes.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfdssetumin_ MATSNESMFDSSETUMIN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfdssetumin_ matsnesmfdssetumin
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   matsnesmfdssetumin_(Mat A,PetscReal *umin, int *__ierr ){
*__ierr = MatSNESMFDSSetUmin(
	(Mat)PetscToPointer((A) ),*umin);
}
#if defined(__cplusplus)
}
#endif
